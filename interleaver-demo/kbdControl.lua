local utils = require("utils")

local graphPath = actiongraph.ContextGraphPath
local startEvent = graphPath..".startEvent"
local stopEvent = graphPath..".finishedEvent"

actiongraph.EventHandler(stopEvent, function()
    utils.print("Relative displacement done")
end)
local displ = 0.01;
local displacementsMap = {
    a =     Vector( displ,      0,      0),
    d =     Vector(-displ,      0,      0),
    w =     Vector(     0, -displ,      0),
    s =     Vector(     0,  displ,      0),
    [","] = Vector(     0,      0,  displ),
    ["."] = Vector(     0,      0, -displ)
}
actiongraph.RegisterStdInRPCCallback(function(msg)
    if msg.command ~= "ide_user_event" then
        return
    end
    if msg["type"] ~= "keyboard_pressed" then
        return
    end
    if displacementsMap[msg.params.key] == nil then
        return
    end
    local displacementParam = displacementsMap[msg.params.key]
    actiongraph.SetParam(graphPath..".displacement", displacementParam)
    actiongraph.SendEvent(startEvent)
end)