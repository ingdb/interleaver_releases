local demoName = "Pick and place"
local utils = require("utils")

local graphPath = actiongraph.ContextGraphPath
local startEvent = graphPath..".startEvent"
local stopEvent = graphPath..".finishedEvent"

actiongraph.EventHandler(stopEvent, function()
    utils.print("Received stopped event for demo ", demoName)
end)

actiongraph.RegisterStdInRPCCallback(function(msg)
    if msg.command ~= "ide_user_event" then
        return
    end
    if msg["type"] ~= "keyboard_pressed" then
        return
    end
    if msg.params.key ~= 'p' then
        return
    end
    actiongraph.SendEvent(startEvent)
end)