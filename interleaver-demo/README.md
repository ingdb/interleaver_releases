# Interleaver robot demo

## Folder structure

- [Visualisation](./visualisation) folder: contains 3d models to use in 3d visualisation. Models are not ready yet.
- [params.yaml](./params.yaml) actiongraph source file with parameters related to mechanical characteristins of interleaver robot, like physical dimensions, actuator resolution, PID coefs, etc..
- [demos.yaml](./demos.yaml) actiongraph source file, contains 2 subgraphs, `AbsoluteMovement` and `PickPlaceMovement1`. This subgraphs provides demo movements, described later.
- [demo1.lua](./demo1.lua) Handles the IDE keypress events and sends corresponding ActionGraph PC event to robot. The only part, which runs on PC side.
- [main.yaml](./main.yaml) Entry point for ActionGraph program. Initialises hardware, main graph, connects PC-side lua script


## How to use
- install VSCode IDE ActionGraph extension
- set full path to actiongraph_packages in IDE settings (Actiongraph package path)
- open VSCode window demo folder as workspace root
- open main.yaml file
- press "Start ActionGraph" button to start simulation
- when 3d model appears and `CALIBRATION` log entry is printed in the window below, press 's' key to trigger pick-place movement

## Pick and place movement explanation 
Usually, when we want to move some object from one place to another, we want to lift it vertically and place to surface also from above, vertically. Demo trajectory implements this concept and consists of 4 parts:
1) when started, small move-up movement is done, vertically up
2) moving towards the XY middle between pick and place points. Z coordinate of this middle point is slightly above max Z of pick and place points to ensure, that we always above straight line between pick and place points
Speed vector in middle point is parallel to (pickpoint)-(placepoint) line, magnitude is defined by `middleSpeedMag` parameter.
3) moving to slightly above the place point
4) make soft touchdown from above at place point

The demo consists of 2 such pick-and-place trajectories, which are symmetrical and final position is the same as start point.

## Parameters to play with:
in [main.yaml](./main.yaml), lines 108-111
- `pickPoint` 3d coordinates of pick point
- `placePoint` 3d coordinates of place point
- `middleSpeedMag` see above
- `accelerationLimit` global acceleration limit for all movements.

Other low level pick and place movement adjustments can be done in [demos.yaml](./demos.yaml)